package justanotherchessgame.model;

import justanotherchessgame.util.GameResult;
import justanotherchessgame.util.Point;
import java.util.ArrayList;
import java.util.List;

/** as we need a skeleton for the different game modes.
 * The Game model represents the status of the game being played.
 * It contains the Chessboard and the moves history. It also must
 * contain reference to the players, so to notify them about the game
 * progress.
 * Core methods identified so far:
 * requestMove(Move m): receives some move request, which must be validated.
 * if performed successfully, must notify players
 * getValidMoves(Square s): in which Locations can I actually move starting from s?
 * returns a list of moves
 */

/**
 * The Game model represents the status of the game being played.
 * It contains the Chessboard and the moves history. It also must
 * contain reference to the players, so to notify them about the game
 * progress.
 */
public class ClassicGame implements GameModel {
    // Private variables
    private final Player whitePlayer;
    private final Player blackPlayer;
    private ChessboardModel board;
    private MovesHistory history;
    private boolean running;

    @Override
    public final boolean isRunning() {
        return running;
    }

    /**
     * Constructor of the Game Model.
     * @param white is the instance of the White player.
     * @param black is the instance of the Black player.
     */
    public ClassicGame(final Player white, final Player black) {
        System.out.println("New game created.");
        running = false;
        history = new MovesHistoryImpl();
        // Builds chessboard as default one
        board = new ChessboardModelImpl(true);
        whitePlayer = white;
        blackPlayer = black;
        // Must be done last, in order to allow players to ask about game informations
        whitePlayer.setGame(this);
        blackPlayer.setGame(this);
    }

    /**
     * Method that checks if the game should end now and eventually ends it.
     */
    protected void checkGameEnd() {
        if (!running) {
            return;
        }
        // Are there any moves available now?
        boolean hasMoves = false;
        for (final Piece p : board.getPieceOnBoard()) {
            if (p.isWhite() == nextColor() && MovesChecker.possibleMoves(board, p.getPoint()).size() > 0) {
                hasMoves = true;
                break;
            }
        }
        // Either checkmate or stalemate
        if (!hasMoves) {
            System.out.println("gameover");
            if (MovesChecker.kingCheck(board, board.getKing(nextColor()).getPoint(), nextColor())) {
                endGame(nextColor() ? GameResult.BLACK_WIN : GameResult.WHITE_WIN);
                // it's a checkmate
            } else {
                endGame(GameResult.STALEMATE);
            }
        } else {
            System.out.println("game still going");
        }
    }

    /**
     * Function used to end the game.
     * @param result is the game result.
     */
    protected void endGame(final GameResult result) {
        if (running) {
            whitePlayer.notifyResult(result);
            blackPlayer.notifyResult(result);
            running = false;
        }
    }

    @Override
    public final boolean nextColor() {
        return history.nextColor();
    }

    @Override
    public final void requestMove(final MoveInfo m) {
        // If game is over, ignore request
        if (!running) {
            return;
        }
        System.out.println("requested " + m);
        // If move is outside board
        if (!m.getTo().onBoard() || !m.getFrom().onBoard()) {
            return;
        }
        //Check turn validity
        if (board.getSquare(m.getFrom()).isWhite() != history.nextColor()) {
            return;
        }
        if (board.move(m)) {
            applyMove(m);
        }
    }

    /**
     * Function used to apply a move on a chessboard.
     * @param m is the moves applied.
     */
    protected void applyMove(final MoveInfo m) {
        history.addMove(m);
        //notifies white and black separately
        whitePlayer.notifyMove(m);
        blackPlayer.notifyMove(m);
        //Check for stalemate/checkmate
        checkGameEnd();
    }

    @Override
    public final List<MoveInfo> getValidMoves(final Point p) {
        //Only if proper turn
        final Piece pi = board.getSquare(p);
        if (pi == null || pi.isWhite() != history.nextColor()) {
            return new ArrayList<MoveInfo>();
        }
        return MovesChecker.possibleMoves(board, p);
    }

    @Override
    public final ChessboardModel getCurrentBoard() {
        return new ChessboardModelImpl(history.getMoves());
    }

    @Override
    public final List<MoveInfo> getMoveHistory() {
        return history.getMoves();
    }
 
    @Override
    public final void loadGame(final List<MoveInfo> moves) {
        for (final MoveInfo m : moves) {
            if (board.move(m)) {
                history.addMove(m);
            } else {
                System.out.println("Invalid moves detected. Board will be set to default");
                board = new ChessboardModelImpl(true);
                history = new MovesHistoryImpl();
                return;
            }
        }
    }

    @Override
    public final int getMovesCount() {
        return history.getMoves().size();
    }

    @Override
    public void start() {
        running = true;
        // Check if game is already over
        checkGameEnd();
        // Notifies the player about the gameStart, shall they do something about it
        whitePlayer.notifyStart();
        blackPlayer.notifyStart();
        System.out.println("Game started");
    }

    @Override
    public void stopGame() {
        // Change game status to ended
        running = false;
        System.out.println("Game was stopped.");
    }
}
