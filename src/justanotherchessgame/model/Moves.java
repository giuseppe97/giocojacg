package justanotherchessgame.model;

/**
 * Enum representing moves of each piece.
 */
public enum Moves {

    /**
     * Move up.
     */
    UP(0, 1), 

    /**
     * Move down.
     */
    DOWN(0, -1),

    /**
     * Move left.
     */
    LEFT(-1, 0), 

    /**
     * Move right.
     */
    RIGHT(1, 0), 

    /**
     * Move diagonal up left.
     */
    UP_LEFT(-1, 1), 

    /**
     * Move diagonal up right.
     */
    UP_RIGHT(1, 1), 

    /**
     * Move diagonal down left.
     */
    DOWN_LEFT(-1, -1), 

    /**
     * Move diagonal down right.
     */
    DOWN_RIGHT(1, -1), 

    /**
     * Move double up.
     */
    UP2(0, 2),

    /**
     * Move double down.
     */
    DOWN2(0, -2), 

    /**
     * Knight up left move.
     */
    KNIGHT_UPL(-1, 2),

    /**
     * Knight up right move.
     */
    KNIGHT_UPR(1, 2),

    /**
     * Knight left up move.
     */
    KNIGHT_LEFTU(-2, 1),

    /**
     * Knight left down move.
     */
    KNIGHT_LEFTD(-2, -1),

    /**
     * Knight right up move.
     */
    KNIGHT_RIGHTU(2, 1),

    /**
     * Knight right down move.
     */
    KNIGHT_RIGHTD(2, -1),

    /**
     * Knight down left move.
     */
    KNIGHT_DOWNL(-1, -2),

    /**
     * Knight down right move.
     */
    KNIGHT_DOWNR(1, -2);

    private int x;
    private int y;

    /**
     * Getter of the x of the move.
     * @return an int representing the x of the move.
     */
    public int getX() {
        return x; 
    }

    /**
     * Getter of the y of the move.
     * @return an int representing the y of the move.
     */
    public int getY() {
        return y; 
    }

    /**
     * Constructor of the move.
     * @param x is the x of the move.
     * @param y is the y of the move.
     */
    Moves(final int x,  final int y) {
        this.x = x;
        this.y = y;
    }

}
