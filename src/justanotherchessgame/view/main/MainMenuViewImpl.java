package justanotherchessgame.view.main;


import justanotherchessgame.controller.MainMenuController;
import justanotherchessgame.model.Main;
import justanotherchessgame.util.ImageGenerator;
import justanotherchessgame.view.AbstractMenuContainer;
import justanotherchessgame.view.AbstractMenuLine;
import justanotherchessgame.view.ResizableGraphicComponent;

import java.io.File;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Class used to create the menu view.
 */
public class MainMenuViewImpl implements MainMenuView, ResizableGraphicComponent {

    //Graphic components needed to resize
    private Pane root;
    private final Title title = new Title("J-A-C-G");
    private AbstractMenuLine normalMode;
    private AbstractMenuLine newGame;
    private AbstractMenuLine blitzMode;
    private AbstractMenuLine challengeMode;
    private AbstractMenuLine loadGame;
    private AbstractMenuLine exit;
    private AbstractMenuLine duoLocal;
    private AbstractMenuLine online;
    private AbstractMenuLine singlePlayer;
    private AbstractMenuContainer hiddenBox;
    private AbstractMenuContainer hiddenBox2;
    private AbstractMenuContainer vbox;
    private ImageView img;
    private final MainMenuController controller;
    /**
     * Class controller.
     * @param ctrl is the controller used to interact with the class.
     */
    public MainMenuViewImpl(final MainMenuController ctrl) {
        controller = ctrl;
    }

    /**
     * Function used to hide both hidden menu after they are shown.
     * @param node is the node which containing the menu.
     * @param choice is a boolean used to determine which menu must be hidden.
     */
    private void removeMenu(final ObservableList<Node> node, final boolean choice) {
        if (choice) {
            node.remove(hiddenBox);
        } else {
            node.remove(hiddenBox2);
        }
    }

    /**
     * Function used to generate the file picker when LoadGame button is clicked.
     * @param stage is the stage where the new window will be shown.
     */
    private void generateFileChooser(final Stage stage) {
        final FileChooser fileChooser = new FileChooser();

        //Set extension filter
        final FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("xml files (*.xml)", "xml");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        final File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            controller.setNew(file);
            createLoadingPopup();
        } else {
            controller.setNew(null);
        }
    }

    @Override
    public final Pane createContent() {
        //create the root of the view
        root = new Pane();
        root.setId("rootPane");
        img = ImageGenerator.generateImage("bg.jpg");
        root.getChildren().add(img);
        //////MENU LINES OF HIDDEN MENUS

        normalMode = new MainMenuLine("NORMAL", false);
        normalMode.setOnMousePressed(e -> {
            if (!root.getChildren().contains(hiddenBox2) && root.getScene() != null) {
                root.getChildren().add(hiddenBox2);
            }
            //set the game mode
            controller.setBlitzMode(false);
        });
        blitzMode = new MainMenuLine("BLITZ", false);
        blitzMode.setOnMousePressed(e -> {
            removeMenu(root.getChildren(), true);
            removeMenu(root.getChildren(), false);
            //set the game mode
            controller.setBlitzMode(true);
            createLoadingPopup();
        });

        challengeMode = new MainMenuLine("CHALL", false);
        duoLocal = new MainMenuLine("DUO LOCAL", false);
        duoLocal.setOnMousePressed(e -> {
            removeMenu(root.getChildren(), true);
            removeMenu(root.getChildren(), false);
            //createPopupStage(false, false);
            //TODO : fix this crap
            //controller.createNewBlitzGame();
            controller.setIA(0);
            createPopupStage(false);
        });
        online = new MainMenuLine("ONLINE", false);
        online.setOnMousePressed(e -> {
        });
        singlePlayer = new MainMenuLine("SOLO", false);
        singlePlayer.setOnMousePressed(e -> {
            removeMenu(root.getChildren(), true);
            removeMenu(root.getChildren(), false);
            createPopupStage(true);
            //controller.createNewAIGame();
        });
        ////END MENU LINES OF HIDDEN MENUS

        //Create both hidden menus
        hiddenBox = new MainMenuContainer(2, normalMode, blitzMode, challengeMode);
        hiddenBox2 = new MainMenuContainer(3, singlePlayer, duoLocal, online);
        // creating the main menu of the view
        newGame = new MainMenuLine("NEW GAME", true);
        newGame.setOnMouseClicked(e -> {
            if (!root.getChildren().contains(hiddenBox) && root.getScene() != null) {
                root.getChildren().add(hiddenBox);
                root.getChildren().remove(hiddenBox2);
            }
            controller.setNew(null);
        });
        loadGame = new MainMenuLine("LOAD GAME", true);
        loadGame.setOnMouseClicked(e -> {
            if (root.getChildren().contains(hiddenBox) && root.getScene() != null) {
                root.getChildren().remove(hiddenBox);
                root.getChildren().remove(hiddenBox2);
            }
            generateFileChooser(Main.getStage());
        });
        exit = new MainMenuLine("EXIT", true);
        exit.setOnMouseClicked(e -> System.exit(0));
        vbox = new MainMenuContainer(1, newGame, loadGame, exit);
        root.getChildren().addAll(title, vbox);
        return root;
    }

    @Override
    public final void resize() {
        //resizing and moving title
        title.resize();
        //resizing background
        img.setFitHeight(Main.getStageHeight());
        img.setFitWidth(Main.getStageWidth());
        //resizing menu container
        vbox.resize();
        if (hiddenBox != null) {
            hiddenBox.resize();
        }
        if (hiddenBox2 != null) {
            hiddenBox2.resize();
        }
        //resizing items
        normalMode.resize();
        newGame.resize();
        blitzMode.resize();
        challengeMode.resize();
        loadGame.resize();
        exit.resize();
        duoLocal.resize();
        singlePlayer.resize();
        online.resize();
    }

    /**
     * Function used to show a popup when the user click on load game.
     */
    private void createLoadingPopup() {
        final Stage dialog = new Stage();
        final GridPane gridPane = new GridPane();
        //game type option
        final ComboBox<String> gameType = new ComboBox<String>();
        final Label gameTypeLabel = new Label("Game Type");
        gameType.getItems().add("Solo");
        gameType.getItems().add("Duo local");
        gameType.getSelectionModel().select(0);
        gridPane.add(gameTypeLabel, 0, 0);
        gridPane.add(gameType, 1, 0);
        final Button btn = new Button("Confirm");
        //depending on the choice, after the click I will show a different popup
        btn.setOnMouseClicked(e -> {
            if (gameType.getSelectionModel().getSelectedItem() == null) {
                System.out.println("Select one mode.");
            } else if (gameType.getSelectionModel().getSelectedItem().equals("Solo")) {
                createPopupStage(true);
            } else {
                //set the ia null
                controller.setIA(0);
                createPopupStage(false);
            }
            dialog.hide();
        });
        //container propriety
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(10));


        gridPane.add(btn, 0, 3, 2, 1);
        GridPane.setHalignment(btn, HPos.CENTER);
        gridPane.setPrefSize(150, 150);

        dialog.initStyle(StageStyle.UTILITY);
        dialog.sizeToScene();
        final Scene scene = new Scene(gridPane);
        dialog.setScene(scene);
        dialog.show();
    }

    /**
     * Function used to show a popup when the user is creating a game, loaded or not.
     * @param doubleOption is a boolean used to know if the popup must show two options (solo mode game) or only one (duo local game).
     * @param isLoading is a parameter used to know if the user is creating a new game or loading one.
     */
    private void createPopupStage(final boolean doubleOption) {
        final Stage dialog = new Stage();
        //container
        final GridPane gridPane = new GridPane();
        //color combobox
        final ComboBox<String> color = new ComboBox<String>();
        color.getItems().add("White");
        color.getItems().add("Black");
        color.getSelectionModel().select(0);
        final Label labelCOlor = new Label("Color");
        //difficult combobox
        final ComboBox<String> difficult = new ComboBox<String>();
        difficult.getItems().add("Easy");
        difficult.getItems().add("Hard");
        difficult.getSelectionModel().select(0);
        final Label labelDifficult = new Label("Difficult");
        //if index is 0, is loading the game
        if (doubleOption) {
            //add difficult and color
            gridPane.add(labelCOlor, 0, 0);
            gridPane.add(color, 1, 0);
            gridPane.add(labelDifficult, 0, 1);
            gridPane.add(difficult, 1, 1);
        } else {
            gridPane.add(labelCOlor, 0, 0);
            gridPane.add(color, 1, 0);
        }
        //confirm button
        final Button btn = new Button("Confirm");
        btn.setOnMouseClicked(e -> {
            controller.setColor(color.getSelectionModel().getSelectedItem().equals("White"));
            if (doubleOption) {
                controller.setIA(difficult.getSelectionModel().getSelectedIndex() + 1);
            }
            controller.createGame();
            dialog.hide();
        });
        //container properties
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(10));

        gridPane.add(btn, 0, 3, 2, 1);
        GridPane.setHalignment(btn, HPos.CENTER);
        gridPane.setPrefSize(300, 300);

        dialog.initStyle(StageStyle.UTILITY);
        dialog.sizeToScene();
        final Scene scene = new Scene(gridPane);
        dialog.setScene(scene);
        dialog.show();
    }
    /**
     * Class used to create the title.
     */
    private static class Title extends StackPane implements ResizableGraphicComponent {
        private final Rectangle bg;
        private final Text text;
        /**
         * Function used to create the title.
         * @param name is text of the title.
         */
        Title(final String name) {
            bg = new Rectangle();
            bg.setStroke(Color.rgb(201, 222, 255));
            bg.setStrokeWidth(2);
            bg.setFill(null);
            text = new Text(name);
            text.setFill(Color.rgb(201, 222, 255));
            setAlignment(Pos.CENTER);
            getChildren().addAll(bg, text);
        }

        @Override
        public void resize() {
            final double width = Main.getStageWidth();
            this.setTranslateX(Main.getStageWidth() * 0.10);
            this.setTranslateY(Main.getStageHeight() * 0.20);
            this.bg.setWidth(width * 0.3);
            this.bg.setHeight(width * 0.3 / 3.5);
            this.text.setFont(Font.font("Terminator Two", FontWeight.SEMI_BOLD, width * 0.2 / 3.5 - 10));
        }
    }
}
