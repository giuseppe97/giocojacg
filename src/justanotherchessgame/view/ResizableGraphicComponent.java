package justanotherchessgame.view;

/**
 * Interface implemented by all graphic components that must be resized.
 */
public interface ResizableGraphicComponent {

    /**
     * Function used to resize the graphic component.
     */
    void resize();
}
