package justanotherchessgame.ai;

import justanotherchessgame.model.ChessboardModel;
import justanotherchessgame.model.MoveInfo;

/**
 * Interface used to represent an Artificial intelligence.
 * @author Francesco Boschi
 *
 */
public interface AIEngine {

    /**
     * Method that produces a valid move for a given player on a given chessboard.
     * @param chessboard is the chessboard state
     * @param color is the player that has to move
     * @return The move found by the engine. Can be null.
     */
    MoveInfo getMove(ChessboardModel chessboard,  boolean color);
}
