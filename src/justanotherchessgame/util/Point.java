package justanotherchessgame.util;

/**
 * This class is instanciated each time we need to save some coordinates onn the chessboard.
 */
public class Point {
    private static final int BOARD_SIZE = 8;
    private int x;
    private int y;

    /**
     * Constructor of the class.
     * @param x represents the x coordinate.
     * @param y represents the y coordinate.
     */
    public Point(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Constructor which creates a copy of a given point.
     * @param point represents the Point to copy.
     */
    public Point(final Point point) {
        x = point.getX();
        y = point.getY();
    }

    /**
     * GETTER of the x variable.
     * @return the x variable of the point.
     */
    public final int getX() {
        return x; 
    }

    /**
     * SETTER of the x variable.
     * @param x represents the x coordinate.
     */
    public final void setX(final int x) {
        this.x = x; 
    }

    /**
     * GETTER of the y variable.
     * @return the y variable of the point.
     */
    public final int getY() {
        return y; 
    }

    /**
     * SETTER of the y variable.
     * @param y represents the y coordinate.
     */
    public final void setY(final int y) {
        this.y = y; 
    }

    /**
     * Method used to check if the instantiated point has legitimate coordinates.
     * @return a boolean True if the point has legitimate coordinates.
     */
    public final boolean onBoard() {
        return (x < BOARD_SIZE && x >= 0 && y < BOARD_SIZE && y >= 0);
    }

    /**
     * Method used to sum two points.
     * @param a is the first addend.
     * @param b is the second addend.
     * @return a Point representing the sum of the two points given.
     */
    public static Point sum(final Point a, final Point b) {
        return new Point(a.getX() + b.getX(), a.getY() + b.getY());
    }

    /**
     * Method used to sum two points.
     * @param a is the Point.
     * @param b is the Point to subtract.
     * @return a Point representing the difference of the two points given.
     */
    public static Point subtract(final Point a, final Point b) {
        return new Point(a.getX() - b.getX(), a.getY() - b.getY());
    }

    /**
     * Method used to multiply a point for a value.
     * @param a is the Point.
     * @param k is the value for which we will multiply the point.
     * @return a Point representing the multiplication of the two points given.
     */
    public static Point mul(final Point a, final int k) {
        return new Point(a.getX() * k, a.getY() * k);
    }

    @Override
    public final boolean equals(final Object obj) {
        return (obj instanceof Point && this.x == ((Point) obj).x && this.y == ((Point) obj).y);
    }

    @Override
    public final int hashCode() {
        return 0;

    }

    @Override
    public final String toString() {
        return "(" + x + ", " + y + ")";
    }
}
