package justanotherchessgame.controller;

import java.io.File;
import javafx.scene.layout.Pane;
import justanotherchessgame.model.AIPlayer;
import justanotherchessgame.model.Player;
import justanotherchessgame.util.FileManagement;
import justanotherchessgame.view.main.MainMenuView;
import justanotherchessgame.view.main.MainMenuViewImpl;
import justanotherchessgame.model.LocalPlayer;
import justanotherchessgame.ai.AIEngine;
import justanotherchessgame.ai.DumbAI;
import justanotherchessgame.ai.MinMaxAI;
import justanotherchessgame.model.BlitzGame;
import justanotherchessgame.model.GameModel;
import justanotherchessgame.model.ClassicGame;

/**
 * This class is interposed between user and main menu view, and it's able to manage all the interactions between user and view.
 */
public class MainMenuControllerImpl implements MainMenuController {

    private final MainMenuView view;
    private boolean blitzMode;
    private File gameFile;
    private boolean localColor;
    private int aiType;

    /**
     * Class constructor.
     */
    public MainMenuControllerImpl() {
        view = new MainMenuViewImpl(this);
        blitzMode = false;
        gameFile = null;
        localColor = true;
        aiType = 0;
    }

    @Override
    public final Pane createMainMenuView() {
        return view.createContent();
    }

    @Override
    public final void createGame() {
        GameModel game;
        Player white;
        Player black;
        // Check for solo vs IA
        if (aiType == 0) {
            white = new LocalPlayer(true);
            black = new LocalPlayer(false);
            new ControllerImpl((LocalPlayer) white, (LocalPlayer) black);
        } else {
            final LocalPlayer local = new LocalPlayer(localColor);
            final AIEngine ai = aiType == 1 ? new DumbAI() : new MinMaxAI(4);
            final AIPlayer opponent = new AIPlayer(!localColor, ai);
            if (localColor) {
                white = local;
                black = opponent;
                new ControllerImpl(local, null);
            } else {
                black = local;
                white = opponent;
                new ControllerImpl(null, local);
            }
        }
        // Check for blitz mode
        if (blitzMode) {
            game = new BlitzGame(white, black, 180, 5);
        } else {
            game = new ClassicGame(white, black);
        }
        // Check for loading
        if (gameFile != null) {
            game.loadGame(FileManagement.loadFromFile(gameFile));
        }
        // Game is ready to start
        game.start();
    }

    @Override
    public final void resize() {
        view.resize();
    }

    @Override
    public final void setBlitzMode(final boolean mode) {
        System.out.println("Mode is now" + mode);
        this.blitzMode = mode;
    }

    @Override
    public final void setNew(final File gameFile) {
        System.out.println("File is now" + gameFile);
        this.gameFile = gameFile;
    }

    @Override
    public final void setColor(final boolean color) {
        System.out.println("Color is now" + color);
        this.localColor = color;
    }

    @Override
    public final void setIA(final int ai) {
        System.out.println("AI is now" + ai);
        this.aiType = ai;
    }
}
